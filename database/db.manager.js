const Sequelize = require('sequelize');
const sequelizeConnection = require('./db.connection');


const UserModel = require('../models/user.model');

const User = UserModel(sequelizeConnection, Sequelize);


const model = {
    User: User
}
const db = {
    ...model,
    sequelizeConnection
}

module.exports = db;