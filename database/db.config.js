const dbconfig = {
    HOST: "34.67.135.134",
    USER: "test",
    PASSWORD: "P4ssw0rd",
    DB: "test",
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}

module.exports = dbconfig;