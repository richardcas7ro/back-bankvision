var express = require('express');
var router = express.Router();
const userController = require("../controllers/user.controller");

/* GET users listing. */
router.post('/', userController.createUser);
router.get('/', userController.findallusers);
router.get('/:id', userController.findUserById);
router.get('/nombre/:nombre', userController.findUserByname);
router.delete('/:id', userController.DeleteByIdusuario);
router.delete('/nombre/:nombre', userController.DeleteByUsername);
router.put('/:id', userController.updateUser);

module.exports = router;