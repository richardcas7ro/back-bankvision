module.exports = (sequelize, Sequelize) => {
    const test = sequelize.define('test',

        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            nombre: {
                type: Sequelize.STRING,
                unique: false
            },
            apellido: {
                type: Sequelize.STRING,
                unique: false
            },
            celular: {
                type: Sequelize.INTEGER,

            },
            fecha_creacion: Sequelize.DATE,

        }, {
            timestamps: false,
            tableName: "test"
        }


    );

    return test;
}