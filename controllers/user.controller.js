const dbmanager = require('../database/db.manager');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

function createUser(req, res) {

    if (!req.body) {
        res.status(400).send({

            message: "NO HAY DATOS QUE INSERTAR"

        });
        return;

    }

    const newUserObject = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        celular: req.body.celular,
        fecha_creacion: req.body.fecha_creacion

    }

    dbmanager.User.create(newUserObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status(500).send({
                    message: " ERROR AL CREAR EL USUARIO"

                }

            );
        }


    );

}

async function findallusers(req, res) {
    try {
        const allusers = await dbmanager.User.findAll();

        res.send({
            data: allusers
        });
    } catch (error) {
        console.log(error)
        res.status(500).send({
            message: "ERROR"
        })
    }
}

async function findUserById(req, res) {
    try {
        const {
            id
        } = req.params;

        const user = await dbmanager.User.findOne(

            {
                where: {
                    id: id
                }
            });

        res.json(user);

    } catch (error) {
        console.log(error, "Error")
        res.status(500).send({
            message: "ERRORR PARA ENCONTRAR EL USUARIO,"
        })

    }
}
async function findUserByname(req, res) {
    try {
        const {
            nombre
        } = req.params;

        const user = await dbmanager.User.findAll(

            {
                where: {
                    nombre: nombre
                }
            });

        res.json(user);

    } catch (error) {
        console.log(error, "Error")
        res.status(500).send({
            message: "ERRORR PARA ENCONTRAR EL USUARIO,"
        })

    }
}

async function DeleteByIdusuario(req, res) {
    const {
        id
    } = req.params;

    await dbmanager.User.destroy({
        where: {
            id: id
        }
    })
    res.send({
        message: "Eliminado el usuario con id : " + id
    })
}
async function DeleteByUsername(req, res) {
    const {
        nombre
    } = req.params;

    await dbmanager.User.destroy({
        where: {
            nombre: nombre
        }
    })
    res.send({
        message: "Eliminado el usuario con nombre : " + nombre
    })
}



async function updateUser(req, res) {
    try {
        const {
            id
        } = req.params;

        const update = await dbmanager.User.update(req.body, {

            where: {
                id: id

            }


        });

        if (update) {
            const updatedUser = await dbmanager.User.findOne({
                where: {
                    id: id
                }
            });

            return res.json(
                updatedUser)


        }
    } catch (error) {
        console.log(error, "Error")
        res.status(500).send({
            message: "Error al Actualizar,"
        })

    }
}








exports.createUser = createUser;
exports.findallusers = findallusers;
exports.findUserById = findUserById;
exports.DeleteByIdusuario = DeleteByIdusuario;
exports.updateUser = updateUser;
exports.findUserByname = findUserByname;
exports.DeleteByUsername = DeleteByUsername;