var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users.route');

const dbManager = require('./database/db.manager');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

dbManager.sequelizeConnection.authenticate().then(
    () => {
        console.log("******* Autenticacion Exitosa con la base de datos de BankVision, BIENVENIDO*************");
        dbManager.sequelizeConnection.sync().then(
            () => {
                console.log("Base de datos sincronizada");
            }
        );
    }
).catch(
    error =>{
        console.log("IMPOSIBLE CONECTAR CON LA BASE DE DATOS, EL ERROR ES EL SIGUIENTE: ",error);
    }
)

module.exports = app;